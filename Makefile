
INCLUDE= -I$(CROSS_COMPILE_PREFIX)/usr/include
LIBPATH= -L$(CROSS_COMPILE_PREFIX)/usr/lib

#ifeq ($(CROSS_COMPILE),)
#CC:= gcc -Wall -g -DPCDEBUG
#LIBLINK= -lpthread
#else
CC:= $(CROSS_COMPILE)g++ -Wall -g
LIBLINK= -lwiringPi -lpthread
#endif

all: pistatdaemon

pistatdaemon: pistatdaemon.o ./socket/socket.o PiStatControl.o PiStatHw.o
	$(CC) -o $@ $^ $(LIBPATH) $(LIBLINK)

pistatdaemon.o: pistatdaemon.cpp ./socket/socket.h

.cpp.o:
	$(CC) $(INCLUDE) -c $< 

clean: 
	rm *.o
	rm pistatdaemon
