#pragma once
#include <time.h>
#include "PiStatHwBase.h"

class PiStatControl {

private:
	enum mode_t {OFF, HEAT, AUX, COOL, FAN} mode_;
	enum state_t {IDLE, RUNNING} state_;
	PiStatHwBase &hw_;
	float setTemp_;		// User set point
	float auxTemp_;		// Point at which aux heat kicks in
	float hysterisis_;	// Grace window around temperature
	time_t offTime_;	// When compressor was turned off
	time_t onTime_;		// When compressor was turned on
	time_t fanRunTime_;	// How long to let the fan run after the compressor turns off
	time_t compressorHoldTime_; // How long to wait after compressor is off to turn it back on
	// Schedule
	// Forecast
	// Logging
	//
	
	void runOff();
	void runHeat();
	void runCool();
	void runFan();
	void runAuxHeat();

public:
	PiStatControl(PiStatHwBase &hw);
	~PiStatControl();
	void runOnce();

	void setSetTemp(float t) { setTemp_=t; }
	void setAuxTemp(float t) { auxTemp_=t; }
	void setMode(mode_t m);
};
