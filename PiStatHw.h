#pragma once
#include "PiStatHwBase.h"

class PiStatHw : public PiStatHwBase {

private:
	enum outputs_t {COMPRESSOR=0,
			AUX=1,
		       	COOL=2,
		       	FAN =3,
	};
	enum regs_t {I2C_REG_AMBIENT_TEMP=5,
	};
	enum address_t {I2C_ADDR_TEMP=0x18,
	};
	int tempFd_;
public:
	PiStatHw();
	~PiStatHw();
	float getTemperature();
	void setCompressor(bool on);
	void setAuxHeat(bool on);
	void setCool(bool on);
	void setFan(bool on);
};

