#pragma once

class PiStatHwBase {
protected:
	bool auxHeat_;
	bool compressor_;
	bool cool_;
	bool fan_;

public:
	virtual ~PiStatHwBase(){};
	virtual float getTemperature()=0;
	virtual void setCompressor(bool on)=0;
	virtual void setAuxHeat(bool on)=0;
	virtual void setCool(bool on)=0;
	virtual void setFan(bool on)=0;

	bool getCompressor() { return compressor_; }
	bool getCool() { return cool_; }
	bool getFan() { return fan_; }
	bool getAuxHeat() { return auxHeat_; }
};

