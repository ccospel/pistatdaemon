import java.io.*;
import java.net.*;
import java.util.*;
import sun.misc.Signal;
import sun.misc.SignalHandler;

public class clienttest 
{
	private static boolean run=true;
	private static String host = "127.0.0.1";
	private static int port=5000;

	private static void usage()
	{
		System.out.println("Usage: ");
		System.out.println(" -p      --port  portno    :  Socket port");
		System.out.println(" -i      --ip    ipaddr    :  Server ip address");
		System.out.println(" -h      --help            :  This help menu");
	}

	private static void parse_command_line(String[] args)
	{
		for(Iterator<String> i = Arrays.asList(args).iterator(); i.hasNext(); ) {
    			String str = i.next();
			if (str.equals("-p") || str.equals("--port"))
			{
				if (!i.hasNext())
				{
					System.out.println(str+" parameter without any data");
					usage();	
					System.exit(1);
				}
				port=Integer.parseInt(i.next());
			}
			else if (str.equals("-i") || str.equals("--ip"))
			{
				if (!i.hasNext())
				{
					System.out.println(str+" parameter without any data");
					usage();	
					System.exit(1);
				}
				host=i.next();
			}
			else if (str.equals("-h") || str.equals("--help"))
			{
				usage();	
				System.exit(0);
			}
		}
	}

	public static void main (String[] args) throws IOException
	{
		Signal.handle(new Signal("INT"), new SignalHandler () {
      			public void handle(Signal sig) { run=false;  }});
	
		try{
		System.out.println("clienttest socket testing program:");
		parse_command_line(args);

		System.out.println("Connecting to the server ["+host+"] on port " + port + " ...");
		
		Socket sock = new Socket(host, port);
		PrintWriter sock_out = new PrintWriter(sock.getOutputStream(), true);
		BufferedReader sock_in = new BufferedReader(
					new InputStreamReader(sock.getInputStream()));
		//  open up standard input
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int i=0;
		System.out.println("Enter command [ctrl-c to quit]:\n\n");
		while (true==run)
		{
			System.out.print("->");
			String input=br.readLine();
			sock_out.println(input);
		}
		} catch (Exception e) {
        		System.out.println("Exception: " + e); 
    		}
	}		
}

