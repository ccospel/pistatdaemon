#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "socket.h"

// Globals
char run=1;			// Signal to exit while loop
int g_port=5000;

// Signal handler
void signal_handler(int sig)
{
	run=0;
}

// Setup the signal handler
void setup_signal_handler()
{
	struct sigaction act;
	act.sa_handler = signal_handler;
	act.sa_flags = 0;

	// Trying to catch user caused exit - to finish gracefully
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGINT, &act, NULL);
}

// Log errors
void error(char *what)
{
	fprintf(stderr, "ERROR: %s\n", what);
	fprintf(stderr, " errno %i [%s]\n", errno, strerror(errno));
	exit(1);
}
// Print an error and exit
void error_exit(char *what)
{
	error(what);
	exit(1);
}

void usage()
{
	printf("servertest socket test program\n");
	printf("Usage:\n");
	printf("-h     --help          -  Usage\n"); 
	printf("-p     --port  port    -  Socket port\n");
}

void parse_command_line(int args, char **argv)
{
	int i;
	for (i=1; i<args; i++)
	{
		// Socket port select
		if (!strcmp(argv[i], "-p") || !strcmp(argv[i], "--port"))
		{
			if (++i==args)
			{
				error("-p parameter without any data");
				usage();
				exit(1);
			}
			g_port=atoi(argv[i]);
		}
		// Socket port select
		else if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
		{
			if (++i==args)
			{
				usage();
				exit(0);
			}
			g_port=atoi(argv[i]);
		}
	}
}

int main(int args, char **argv)
{
	parse_command_line(args, argv);
	setup_signal_handler();

	printf("Starting the socket server...\n");
	int server=socket_setup_server(5000);
	if (server < 0)
		error_exit("Failed to start the server");	
	while (run)
	{
		printf("Waiting for a connection...");
		int conn=-1;
		while (run&&conn==-1)
		{
			if ((conn=accept(server, (struct sockaddr*)NULL, NULL))>=0)
				printf("Connection accepted\n");
			else
				usleep(200000);
		}	

		printf("Reading:\n\n");
		char buff[1024];
		while (run && conn!=-1)
		{
			if (socket_select(conn, 100000))
			{
				int n=read(conn, buff, sizeof(buff)-1);
				if (n <= 0)
				{
					printf("Disconnected\n");
					conn=-1;
				}
				else
				{
					buff[n]=0;
					printf("%s\n", buff);
				}
			}
		}
	}
	
	return 0;
}
