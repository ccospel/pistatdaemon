#include <netinet/in.h>
#include <string.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include "socket.h"

 
int socket_setup_server(int port)
{
	int sfd;
	struct sockaddr_in saddr;
        sfd= socket(AF_INET, SOCK_STREAM|SOCK_NONBLOCK, 0);
	memset(&saddr, '0', sizeof(saddr));
        saddr.sin_family=AF_INET;           /* Set Address Family to Internet */
        saddr.sin_addr.s_addr=htonl(INADDR_ANY);    /* Any Internet address */
        saddr.sin_port=htons(port);            
        bind(sfd, (struct sockaddr *)&saddr, sizeof(saddr));
        listen(sfd, 1);

	return sfd;
}

int socket_setup_client(int port, char ip[])
{
	int fd=-1;
	struct sockaddr_in serv_addr; 

	if((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		return fd;
	memset(&serv_addr, '0', sizeof(serv_addr)); 
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port); 

	if(inet_pton(AF_INET, ip, &serv_addr.sin_addr)<=0)
		return -1;
	
	if( connect(fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
		return -1;
	return fd;
}
	
int socket_select(int fd, time_t timeout)
{
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	struct timeval ptval;
	ptval.tv_sec=0;
	ptval.tv_usec=timeout;

	if (select(fd+1, (fd_set*)0, &fds, (fd_set*)0, &ptval)==-1)
		return -1;
	if (!FD_ISSET(fd, &fds))
		return 0;
	return 1;
}
