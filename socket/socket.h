#pragma once
#include <sys/socket.h>

int socket_setup_server(int port);
int socket_setup_client(int port, char ip[]);
int socket_select(int fd, time_t);
