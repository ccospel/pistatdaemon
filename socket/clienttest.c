#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "socket.h"

// Globals
char run=1;			// Signal to exit while loop
int g_port=5000;
char g_ip[255] = "127.0.0.1";

// Signal handler
void signal_handler(int sig)
{
	run=0;
}

// Setup the signal handler
void setup_signal_handler()
{
	struct sigaction act;
	act.sa_handler = signal_handler;
	act.sa_flags = 0;

	// Trying to catch user caused exit - to finish gracefully
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGINT, &act, NULL);
}

// Log errors
void error(char *what)
{
	fprintf(stderr, "ERROR: %s\n", what);
	fprintf(stderr, " errno %i [%s]\n", errno, strerror(errno));
	exit(1);
}
// Print an error and exit
void error_exit(char *what)
{
	error(what);
	exit(1);
}

void usage()
{
	printf("Usage:\n");
	printf("-h     --help          -  Usage\n"); 
	printf("-p     --port  port    -  Socket port\n");
	printf("-i     --ip    port    -  Server IP\n");
}

void parse_command_line(int args, char **argv)
{
	int i;
	for (i=1; i<args; i++)
	{
		// Socket port select
		if (!strcmp(argv[i], "-p") || !strcmp(argv[i], "--port"))
		{
			if (++i==args)
			{
				error("-p parameter without any data");
				usage();
				exit(1);
			}
			g_port=atoi(argv[i]);
		}
		else if (!strcmp(argv[i], "-i") || !strcmp(argv[i], "--ip"))
		{
			if (++i==args)
			{
				error("-i parameter without any data");
				usage();
				exit(1);
			}
			strcpy(g_ip, argv[i]);
		}
		// Socket port select
		else if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
		{
			usage();
			exit(0);
		}
	}
}

int main(int args, char **argv)
{
	printf("clienttest socket testing program:\n");
	parse_command_line(args, argv);
	setup_signal_handler();

	printf("Connecting to the server [%s] on port %i...\n", g_ip, g_port);
	int sock=socket_setup_client(g_port, g_ip);
	if (sock < 0)
		error_exit("Failed to connect");

	printf("Enter command [ctrl-c to quit]:\n\n");
	char buff[1024];
	while (run)
	{
		printf("-> ");
//		scanf("%[]s", buff);
		fgets(buff, 1024, stdin);
		int n=write(sock, buff, strlen(buff));
		sleep(1);
	}
	close(sock);
	
	return 0;
}
