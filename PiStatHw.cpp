#include "PiStatHw.h"
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <string.h>
#include <stdexcept>
#include <string>
using namespace std;

#ifndef PCDEBUG
#include <wiringPi.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#else
// Debugging 
#define PWM_OUTPUT 10
#define OUTPUT 0
#define INPUT 1
void pinMode(char a, char b) { printf("%s %i %i\n", __func__, a, b); }
int wiringPiSetup() { printf("%s\n", __func__); return 0; }
int pwmWrite(int a, int b) { printf("%s %i %i\n", __func__, a, b); return 0;}
#endif

PiStatHw::PiStatHw() {
	// Setup
	if (-1 == wiringPiSetup())
		throw runtime_error("wiringPiSetup() failed");

	// Setup pinmuxes 
	pinMode(COMPRESSOR, PWM_OUTPUT);
	pinMode(AUX, PWM_OUTPUT);
	pinMode(COOL, PWM_OUTPUT);
	pinMode(FAN, PWM_OUTPUT);

	// Setup I2c
	string path="/dev/i2c-1";
	tempFd_ = open(path.c_str(), O_RDWR);
	if (-1 == tempFd_)
		throw runtime_error("error opening "+path);
	ioctl(tempFd_, I2C_SLAVE, I2C_ADDR_TEMP);
//	if (-1==(fd_adc = wiringPiI2CSetup(I2C_ADDR_ADC)))
//		error_exit("wiringPiI2CSetup() failed [ADC]");

	auxHeat_=0;
	compressor_=0;
	cool_=0;
	fan_=0;
}

PiStatHw::~PiStatHw() {
	setCompressor(false);
	setAuxHeat(false);
	setCool(false);
	setFan(false);
	close(tempFd_);
}

float PiStatHw::getTemperature() {
	/*
	const int size=1;
	unsigned char buff[32];
	int ret = i2c_smbus_read_i2c_block_data(tempFd_, 0, size, buff);
	if (ret == 0)
		throw runtime_error((string)__func__+"I2C no data returned");
	*/
	int ret = i2c_smbus_read_word_data(tempFd_, I2C_REG_AMBIENT_TEMP);
	if (ret < 0)
		throw runtime_error((string)__func__+"(): I2C error "+strerror(-1*ret));
	// Convert to 13 bit
	if (ret & 0x1000)
		return (ret&0xfff)/16-256;
	return (ret&0xfff)/16;
}

void PiStatHw::setCompressor(bool on)
{
	digitalWrite(COMPRESSOR, on);
	compressor_=1;
}

void PiStatHw::setAuxHeat(bool on)
{
	if (digitalRead(COOL))
		throw runtime_error((string)__func__+"Aux requested while in cooling mode");
	digitalWrite(AUX, on);
	auxHeat_=1;
}

void PiStatHw::setCool(bool on)
{
	if (digitalRead(AUX))
		throw runtime_error((string)__func__+" Cooling requested while in aux heat mode");
	digitalWrite(COOL, on);
	cool_=1;
}

void PiStatHw::setFan(bool on)
{
	digitalWrite(FAN, on);
	fan_=1;
}
