#include "PiStatControl.h"
#include <string>
#include <stdexcept>
using namespace std;

PiStatControl::PiStatControl(PiStatHwBase &hw) : hw_(hw) {
	state_=IDLE;
	offTime_=time(NULL);
	compressorHoldTime_ = 120;
	fanRunTime_=30;
	auxTemp_ = 2;
}

PiStatControl::~PiStatControl() {

}

void PiStatControl::runOnce() {
	switch (mode_) {
		case OFF:	runOff(); break;
		case HEAT:	runHeat(); break;
		case COOL:	runCool(); break;
		case FAN:	runFan(); break;
		case AUX:	runAuxHeat(); break;
		default: 
		throw runtime_error((string)__func__+"Unknown mode");
	}	
}

void PiStatControl::runOff() {
	if (RUNNING == state_)
	{
		hw_.setCompressor(false);
		hw_.setAuxHeat(false);
		hw_.setFan(false);
		hw_.setCool(false);
		state_ = IDLE;
	}
}

void PiStatControl::runHeat() {
	float temp = hw_.getTemperature();
	// If the temperature is low enough, start the heat
	if (temp < (setTemp_ - hysterisis_))
	{
		if (IDLE == state_)
		{
			// Make sure the compressor has been off long enough
			if (time(NULL) < (offTime_+compressorHoldTime_))
				return;

			hw_.setFan(true);
			hw_.setCool(false);
			hw_.setCompressor(true);
			onTime_=time(NULL);
			state_ = RUNNING;
			offTime_=0;		// XXX: Same information as state_?
		}
		// If we're running and the temperature drops low enough,
		// kick on the auxillary heat
		// TODO: check this algo
		if (temp < (setTemp_ - auxTemp_ - hysterisis_))
			hw_.setAuxHeat(true);
		else if (temp > (setTemp_ - auxTemp_))
			hw_.setAuxHeat(false);
	}
	else if (temp > (setTemp_ + hysterisis_) && RUNNING == state_)
	{
		hw_.setCompressor(false);
		hw_.setAuxHeat(false);
		// run the fan for a bit
		if (0==offTime_)
			offTime_ = time(NULL);
		else if (time(NULL) > offTime_+fanRunTime_)
			hw_.setFan(false);
	}
}


void PiStatControl::runCool() {
	float temp = hw_.getTemperature();
	// If the temperature is high enough, start cooling
	if (temp > (setTemp_ + hysterisis_) && IDLE == state_)
	{
		// Make sure the compressor has been off long enough
		if (time(NULL) < (offTime_+compressorHoldTime_))
			return;

		hw_.setFan(true);
		hw_.setCool(true);
		hw_.setCompressor(true);
		onTime_=time(NULL);
		state_ = RUNNING;
		offTime_=0;		// XXX: Same information as state_?
	}
	else if (temp < (setTemp_ - hysterisis_) && RUNNING == state_)
	{
		hw_.setCompressor(false);
		hw_.setCool(false);	// TODO: Is the RevValve call released in production tstats?
		// run the fan for a bit
		if (0==offTime_)
			offTime_ = time(NULL);
		else if (time(NULL) > offTime_+fanRunTime_)
			hw_.setFan(false);
	}
}

void PiStatControl::runAuxHeat() {
	float temp = hw_.getTemperature();
	// If the temperature is low enough, start the heat
	if (temp < (setTemp_ - hysterisis_) && IDLE == state_)
	{
		// Make sure the compressor has been off long enough
		if (time(NULL) < (offTime_+compressorHoldTime_))
			return;

		hw_.setFan(true);
		hw_.setCool(false);
		hw_.setCompressor(false);
		hw_.setAuxHeat(true);
		onTime_=time(NULL);
		state_ = RUNNING;
		offTime_=0;		// XXX: Same information as state_?
	}
	else if (temp > (setTemp_ + hysterisis_) && RUNNING == state_)
	{
		hw_.setCompressor(false);
		hw_.setAuxHeat(false);
		// run the fan for a bit
		if (0==offTime_)
			offTime_ = time(NULL);
		else if (time(NULL) > offTime_+fanRunTime_)
			hw_.setFan(false);
	}
}

void PiStatControl::runFan() {
	if (IDLE == state_)
	{
		hw_.setFan(true);
		state_=RUNNING;
	}
}

void PiStatControl::setMode(const mode_t mode) {
	// If the mode matches, do nothing
	if (mode == mode_)
		return;
	// Shutdown
	hw_.setCompressor(false);
	hw_.setAuxHeat(false);
	hw_.setCool(false);
	hw_.setFan(false);
	// Note the shutdown time
	offTime_=time(NULL);
	// Update the mode variable
	mode_ = mode;
}
