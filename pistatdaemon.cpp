extern "C" {
#include "socket/socket.h"
}
#include "PiStatHw.h"
#include "PiStatControl.h"
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#define VERSION	"0.1"
#define VERSION_DATE "November 14 2013"

// Globals
char g_run=1;			// Signal to exit while loop
int g_port=5000;

// Signal handler
void signal_handler(int sig)
{
	g_run=0;
}

// Setup the signal handler
void setup_signal_handler()
{
	struct sigaction act;
	act.sa_handler = signal_handler;
	act.sa_flags = 0;

	// Trying to catch user caused exit - to finish gracefully
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGINT, &act, NULL);
}

// Log errors
void error(const string &what)
{
	cerr << "Error: " << what << endl;
	cerr << " errno " << errno << " [" << strerror(errno) << "]" << endl;
	exit(1);
}

// Print an error and exit
void error_exit(const string &what)
{
	error(what);
	exit(1);
}

void parse_command(const string &buff)
{
	cout << "Received: " << buff << endl;

	stringstream ss(buff);
	string cmd;
	ss >> cmd;

	if (cmd == "M")
	{
		cout << __func__ << ": got M" << endl;
	}

	// TODO
}

static void * socket_thread(void*)
{
	char buff[1024];
	int server=socket_setup_server(g_port);
	if (server<0)
		error_exit("socket_thread(): error setting up the server");
	while (g_run)
	{
		cout << "socket_thread(): waiting for a connection" << endl;
		int conn=-1;
		while (g_run && conn==-1)
		{
			if ((conn=accept(server, (struct sockaddr*)NULL, NULL))>=0)
				cout << "socket_thread(): Connection accepted" << endl;
			else
				usleep(200000);
		}
		while (g_run && conn!=-1)
		{
			if (socket_select(conn, 100000))
			{
				int n=read(conn, buff, sizeof(buff)-1);
				if (n <= 0)
				{
					cout << "socket_thread(): Connection closed" << endl;
					conn=-1;
				}
				else
				{
					buff[n]=0;
					parse_command(buff);
				}
			}
		}
	}
	close(server);
   	pthread_exit(NULL);
	return NULL;
}

//    digitalWrite (LED, HIGH) ;  // On

void usage()
{
	cout << "PiStatDaemon version " << VERSION << " " << VERSION_DATE << endl;
	cout << "Usage:" << endl;
	cout << " -h     --help          -  Usage" << endl;
	cout << " -p     --port  port    -  Socket port" << endl;
}

void parse_command_line(int args, char **argv)
{
	int i;
	for (i=1; i<args; i++)
	{
		// Socket port select
		if (!strcmp(argv[i], "-p") || !strcmp(argv[i], "--port"))
		{
			if (++i==args)
			{
				error("-p parameter without any data");
				usage();
				exit(1);
			}
			g_port=atoi(argv[i]);
		}
		// Socket port select
		else if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
		{
			usage();
			exit(0);
		}
	}
}

int main(int args, char **argv)
{
	int ret=0;
	PiStatHw *pihw=NULL;
	int control_sleep_time=1000000;//100000;
	parse_command_line(args, argv);
	setup_signal_handler();

	// Create the thread(s)
	long t;
	pthread_t thread_sock;
	pthread_attr_t attr;
  	pthread_attr_init(&attr);
   	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	void *status;
	cout << "Creating the socket_thread..." << endl;
	if (pthread_create(&thread_sock, &attr, socket_thread, (void*)t))
		error_exit("failed to create thread");

	try {
		// Open the HW interface
		cout << "Initializing hardware..." << endl;
		pihw = new PiStatHw();

		cout << "Starting control algorithm..." << endl;
		// Open the controller
		PiStatControl picontrol(*pihw);

		// Main Loop
		cout << "Starting main loop: " << endl;
		while (g_run)
		{
			picontrol.runOnce();
			usleep(control_sleep_time);
		} 
	} catch (const std::exception &e) {
		cerr << "EXCEPTION: " << e.what() << endl;
		ret=1;
	} catch (...) {
		cerr << "EXCEPTION: unknown" << endl;
		ret=1;
	}
	// Signal the thread(s), just in case we are here by exception
	g_run=false;	

	cout << "Deinitializing hardware..." << endl;
	if (pihw)
		delete pihw;
	cout << "Waiting for threads to complete..." << endl;
	pthread_attr_destroy(&attr);
	pthread_join(thread_sock, &status);

	cout << "Exiting..." << endl;
   	pthread_exit(NULL);
	return ret;
}
